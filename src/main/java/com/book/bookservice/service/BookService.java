package com.book.bookservice.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.book.bookservice.entity.Book;
import com.book.bookservice.repo.BookRepo;

@Service
public class BookService {

    @Autowired
    private BookRepo bookRepo;

    public Book getBookByID(int id) {
        return bookRepo.findById(id).get();
    }

    public Book findBookByName(String name) {
        return bookRepo.findByName(name);
    }

    public List<Book> listBooks() {
        return bookRepo.findAll(Sort.by(Sort.Direction.DESC, "id"));
    }

    public Book save(Book book) {
        return bookRepo.save(book);
    }

}
