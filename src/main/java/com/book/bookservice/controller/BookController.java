package com.book.bookservice.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.book.bookservice.entity.Book;
import com.book.bookservice.service.BookService;

@RestController
public class BookController {

    Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @GetMapping("/books")
    public List<Book> listBooks() {
        logger.info("fetching books..");
        return bookService.listBooks();
    }

    @GetMapping("/books/{id}")
    public Book getBookByID(@PathVariable int id) {
        return bookService.getBookByID(id);
    }

    @GetMapping("/books/by_name/{name}")
    public Book findBookByName(@PathVariable String name) {
        return bookService.findBookByName(name);
    }

    @PostMapping("/books")
    public Book createBook(@RequestBody Book book) {
        return bookService.save(book);
    }

}
