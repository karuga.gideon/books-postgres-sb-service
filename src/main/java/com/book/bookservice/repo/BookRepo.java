package com.book.bookservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.book.bookservice.entity.Book;

public interface BookRepo extends JpaRepository<Book, Integer> {

    Book findByName(String name);
}
